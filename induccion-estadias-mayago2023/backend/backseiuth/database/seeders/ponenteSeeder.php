<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ponenteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(table:'ponente')->insert(
        [
            'nombre'=>'Francisco',
            'apellido'=>'Encinas',
            'email'=>'franciscojavier@gmail.com',
            'telefono'=>'6621006181',
            'profesion'=>'Ingeniero en Tecnologías de la Información',
        ]);

        DB::table(table:'ponente')->insert(
        [
            'nombre'=>'Daniel',
            'apellido'=>'Rangel',
            'email'=>'danielrangel@gmail.com',
            'telefono'=>'6621006181',
            'profesion'=>'Ingeniero en Redes Infraestructura',
        ]);

        DB::table(table:'ponente')->insert(
        [
            'nombre'=>'Alan',
            'apellido'=>'Granillo',
            'email'=>'alangranillo@gmail.com',
            'telefono'=>'6621006181',
            'profesion'=>'Ingeniero en Multimedia',
        ]);

        DB::table(table:'ponente')->insert(
        [
            'nombre'=>'Lluvia',
            'apellido'=>'Corral',
            'email'=>'lluviacorral@gmail.com',
            'telefono'=>'6621006181',
            'profesion'=>'Ingeniero en Tecnologías de la Información',
        ]);
    }
}
