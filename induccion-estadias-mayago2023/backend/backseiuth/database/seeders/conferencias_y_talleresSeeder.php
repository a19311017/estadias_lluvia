<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class conferencias_y_talleresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(table:'conferencias_y_talleres')->insert(
            [ 
            'tipoEvento'=>'CONFERENCIA',
            'lugar'=>'AUDITORIO',
            'edificio'=>'E',
            'nombreEvento'=>'Conferencia Blockchain 1',
            'descripcion'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dapibus quam non interdum mattis. Proin tempus at lacus vel venenatis',
            'fecha'=>'2023-06-01',
            'hora'=>'10:00',
            'duracion'=>90,
            'ponente_id'=>1,
            ]);

            DB::table(table:'conferencias_y_talleres')->insert(
            [
            'tipoEvento'=>'CONFERENCIA',
            'lugar'=>'AUDITORIO',
            'edificio'=>'B',
            'nombreEvento'=>'Conferencia Cyberseguridad 2',
            'descripcion'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dapibus quam non interdum mattis. Proin tempus at lacus vel venenatis',
            'fecha'=>'2023-06-01',
            'hora'=>'11:45',
            'duracion'=>90,
            'ponente_id'=>2,
            ]);

            DB::table(table:'conferencias_y_talleres')->insert(
            [
            'tipoEvento'=>'TALLER',
            'lugar'=>'LABORATORIO B',
            'edificio'=>'E',
            'nombreEvento'=>'Hackeando Facebook',
            'descripcion'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dapibus quam non interdum mattis. Proin tempus at lacus vel venenatis',
            'fecha'=>'2023-06-01',
            'hora'=>'14:30',
            'duracion'=>210,
            'ponente_id'=>2,
            ]);

            DB::table(table:'conferencias_y_talleres')->insert(
            [
            'tipoEvento'=>'CONFERENCIA',
            'lugar'=>'AUDITORIO',
            'edificio'=>'B',
            'nombreEvento'=>'Conferencia Desarrollo Híbrido 3',
            'descripcion'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dapibus quam non interdum mattis. Proin tempus at lacus vel venenatis',
            'fecha'=>'2023-06-02',
            'hora'=>'11:30',
            'duracion'=>90,
            'ponente_id'=>3,
            ]);

            DB::table(table:'conferencias_y_talleres')->insert(
            [
            'tipoEvento'=>'TALLER',
            'lugar'=>'LABORATORIO',
            'edificio'=>'K',
            'nombreEvento'=>'Como navegar en la Deep Web',
            'descripcion'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dapibus quam non interdum mattis. Proin tempus at lacus vel venenatis',
            'fecha'=>'2023-06-02',
            'hora'=>'11:30',
            'duracion'=>180,
            'ponente_id'=>4,
            ]);

            
    }
}
