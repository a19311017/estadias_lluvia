<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      

        Schema::create('conferencias_y_talleres', function(Blueprint $table) {
            $table->id();
            $table->text('tipoEvento');
            $table->text('lugar');
            $table->text('edificio');
            $table->text('nombreEvento');
            $table->text('descripcion');
            $table->date('fecha');
            $table->time('hora');
            $table->integer('duracion');
        
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfexists('conferencias_y_talleres');
    }
};
