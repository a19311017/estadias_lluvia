<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventoController;
use App\Http\Controllers\PonenteController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request)
{
    return $request->user();
});

Route::get('pruebas', [EventoController::class, 'index'])->name('prueba_de_eventos');
Route::get('ponentes', [PonenteController::class, 'index'])->name('prueba_de_eventos');

Route::get('conferencias_y_talleres', function ()
{
    return <<<EOT
    {
        "success": true,
        "data":
        [
            {
                "id": 1,
                "tipoEvento": "CONFERENCIA",
                "lugar": "AUDITORIO",
                "edificio": "E",
                "nombreEvento": "Conferencia Blockchain 1",
                "descripcion": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dapibus quam non interdum mattis. Proin tempus at lacus vel venenatis.",
                "fecha":"2023-06-01",
                "hora":"10:00",
                "duracion":90,
                "ponente_id": 1
            },
            {
                "id": 2,
                "tipoEvento": "CONFERENCIA",
                "lugar": "AUDITORIO",
                "edificio": "B",
                "nombreEvento": "Conferencia Cyberseguridad 2",
                "descripcion": "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
                "fecha":"2023-06-01",
                "hora":"11:45",
                "duracion":90,
                "ponente_id": 2
            },
            {
                "id": 3,
                "tipoEvento": "TALLER",
                "lugar": "LABORATORIO B",
                "edificio": "E",
                "nombreEvento": "Hackeando Facebook",
                "descripcion": "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
                "fecha":"2023-06-01",
                "hora":"14:30",
                "duracion":210,
                "ponente_id": 2
            },
            {
                "id": 4,
                "tipoEvento": "CONFERENCIA",
                "lugar": "AUDITORIO",
                "edificio": "B",
                "nombreEvento": "Conferencia Desarrollo Híbrido 3",
                "descripcion": "kjhsdkjfhs kkjlkhlkjhj sdfshdfjsfs jjhkj jhg jhg. Ssdfljh ljhlkjh lkjhkjjk kjhskdfhks kjhlkjhlkj ...",
                "fecha":"2023-06-02",
                "hora":"11:30",
                "duracion":90,
                "ponente_id": 3
            },
            {
                "id": 5,
                "tipoEvento": "TALLER",
                "lugar": "LABORATORIO A",
                "edificio": "K",
                "nombreEvento": "Como navegar en la Deep Web",
                "descripcion": "kjhsdkjfhs kkjlkhlkjhj sdfshdfjsfs jjhkj jhg jhg. Ssdfljh ljhlkjh lkjhkjjk kjhskdfhks kjhlkjhlkj ...",
                "fecha":"2023-06-02",
                "hora":"11:30",
                "duracion":180,
                "ponente_id": 4
            }
        ],
        "message": "Información listada correctamente"
    }
    EOT;
});
