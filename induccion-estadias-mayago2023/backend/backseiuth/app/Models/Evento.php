<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    use HasFactory;

    protected $table="conferencias_y_talleres";

    public function conferencias_y_talleres(){
        return $this->hasMany(Ponente::class);
        
    }
}
